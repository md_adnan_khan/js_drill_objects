function mapObject(object, callback) {

    if(object === null || typeof object !== 'object' || Array.isArray(object)) {
        console.log(`Input is not a valid object !`);
    }

    let newObject = {};

    for(let key in object) {
        newObject[key] = callback(key, object);
    }
    
    return newObject;
}

module.exports.mapObject = mapObject;