const {mapObject} = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

let mapObjectList = mapObject(testObject, ((key, obj) => {

    if(key == 'name') {
        obj[key] = "Thor Ragnarok";
    } else if(key == 'age') {
        obj[key] = 25;
    } else if(key == 'location') {
        obj[key] = "Asguard";
    }
    
    return obj[key];
    
}));

console.log(mapObjectList);