const {pairs} = require('../pairs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

let pairsArray = pairs(testObject, ((key, object) => {
    let innerArray = [];

    innerArray.push(key, object[key]);

    return innerArray;
    
}));

console.log(pairsArray);
