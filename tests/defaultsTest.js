const {defaults} = require('../defaults');

const object = {flavor: "chocolate"};
const defaultProps = {flavor: "vanilla", sprinkles: "lots"};

let resultObject = defaults(object, defaultProps);

console.log(resultObject);