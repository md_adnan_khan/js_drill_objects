function invert(object) {
    let invertObject = {};

    for(let key in object) {
        let keyToInvert = object[key];
        let valueToInvert = key;

        invertObject[keyToInvert] = valueToInvert;
    }

    return invertObject;
}

module.exports.invert = invert;