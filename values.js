function values(object, callback) { 

    if(object === null || typeof object !== 'object' || Array.isArray(object)) {
        console.log(`Input is not a valid object !`);
    }

    let array = [];

    for(let key in object) {
        array.push(callback(key, object));
    } 
    
    return array;
}

module.exports.values = values;