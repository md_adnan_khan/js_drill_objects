function pairs(object, callback) {

    if(object === null || typeof object !== 'object' || Array.isArray(object)) {
        console.log(`Input is not a valid object !`);
    }
    
    let answerArray = [];

    for(let key in object) {
        answerArray.push(callback(key, object));
    }

    return answerArray;
}

module.exports.pairs = pairs;